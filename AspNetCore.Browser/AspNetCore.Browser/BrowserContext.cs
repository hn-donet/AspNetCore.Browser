﻿using AspNetCore.Browser.Protection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Browser
{

    /// <summary>
    /// 浏览器标识上下文
    /// </summary>
    public class BrowserContext : IBrowserContext
    {
        private IBrowserProtection _browserProtection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="browserProtection"></param>
        public BrowserContext(IBrowserProtection browserProtection)
        {
            _browserProtection = browserProtection;
        }

        /// <summary>
        /// 获取browserid，默认cookie名称
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetBrowserID(HttpContext context)
        {
            return GetBrowserID(context, BrowserDefaults.CookieName);
        }

        /// <summary>
        /// 获取browserid，指定cookie名称
        /// </summary>
        public string GetBrowserID(HttpContext context, string cookieName)
        {
            string cookieValue = context.Request.Cookies[cookieName];
            return _browserProtection.Unprotect(cookieValue);
        }




    }



}
