﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Browser.Protection
{
    /// <summary>
    /// broswer加密解密处理
    /// </summary>
    public interface IBrowserProtection
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        string Protect(string data);

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="protectedText"></param>
        /// <returns></returns>
        string Unprotect(string protectedText);
    }
}
