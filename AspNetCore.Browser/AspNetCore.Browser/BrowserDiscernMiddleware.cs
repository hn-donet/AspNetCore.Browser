﻿using AspNetCore.Browser.Protection;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// 浏览器中间件
    /// </summary>
    public class BrowserDiscernMiddleware
    {
        private static readonly RandomNumberGenerator CryptoRandom = RandomNumberGenerator.Create();
        private static readonly int BrowserIDLength = 36; 

        private readonly RequestDelegate _next;
        private BrowserOptions _options;
        private IBrowserProtection _browserProtection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        /// <param name="browserProtection"></param>
        /// <param name="options"></param>
        public BrowserDiscernMiddleware(RequestDelegate next, IBrowserProtection browserProtection, BrowserOptions options)
        {
            _next = next;
            _options = options;
            _browserProtection = browserProtection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            string cookieValue = context.Request.Cookies[_options.Cookie.Name];

            string browserId = _browserProtection.Unprotect(cookieValue);

            if (String.IsNullOrWhiteSpace(browserId) || browserId.Length != BrowserIDLength)
            {
                var guidBytes = new byte[16];
                CryptoRandom.GetBytes(guidBytes);
                cookieValue = _browserProtection.Protect(new Guid(guidBytes).ToString());
                var establisher = new BrowserEstablisher(context, cookieValue, _options);
            }
            await _next.Invoke(context);
        }

        /// <summary>
        /// 
        /// </summary>
        private class BrowserEstablisher
        {
            private readonly HttpContext _context;
            private readonly string _cookieValue;
            private readonly BrowserOptions _options;

            public BrowserEstablisher(HttpContext context, string cookieValue, BrowserOptions options)
            {
                _options = options;
                _context = context;
                _cookieValue = cookieValue;
                context.Response.OnStarting(OnStartingCallback, state: this);
            }

            private static Task OnStartingCallback(object state)
            {
                var establisher = (BrowserEstablisher)state;
                establisher.SetCookie();
                return Task.FromResult(0);
            }

            private void SetCookie()
            {
                var cookieOptions = _options.Cookie.Build(_context);

                _context.Response.Cookies.Append(_options.Cookie.Name, _cookieValue, cookieOptions);

                _context.Response.Headers["Cache-Control"] = "no-cache";
                _context.Response.Headers["Pragma"] = "no-cache";
                _context.Response.Headers["Expires"] = "-1";
            }
        }

    }
     
    

    

}
