﻿using AspNetCore.Browser;
using AspNetCore.Browser.Protection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 
    /// </summary>
    public static class BrowserServiceCollectionExtensions
    {
        /// <summary>
        /// 添加浏览器标识的服务
        /// 得启用 services.AddDataProtection() 如果没有，请手动添加
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddBrowserDiscern(this IServiceCollection services)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<IBrowserProtection, BrowserProtection>();
            services.AddSingleton<IBrowserContext, BrowserContext>();
            services.AddSingleton<BrowserOptions>(new BrowserOptions());
            return services;
        }

        /// <summary>
        /// 添加浏览器标识的服务
        /// 得启用 services.AddDataProtection() 如果没有，请手动添加
        /// </summary>
        /// <param name="services"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IServiceCollection AddBrowserDiscern(this IServiceCollection services, Action<BrowserOptions> action)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<IBrowserProtection, BrowserProtection>();
            services.AddSingleton<IBrowserContext, BrowserContext>();

            BrowserOptions options = new BrowserOptions();
            action(options);
            //
            services.AddSingleton<BrowserOptions>(options);
            return services;
        }

    }
}
