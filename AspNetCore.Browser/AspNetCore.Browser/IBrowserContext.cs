﻿using Microsoft.AspNetCore.Http;
using System;

namespace AspNetCore.Browser
{
    /// <summary>
    /// 浏览器标识上下文
    /// </summary>
    public interface IBrowserContext
    {
        /// <summary>
        /// 获取browserid，默认cookie名称
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        string GetBrowserID(HttpContext context);

        /// <summary>
        /// 获取browserid，指定cookie名称
        /// </summary>
        string GetBrowserID(HttpContext context, string cookieName);
    }

}
